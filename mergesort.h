#include<vector>

using namespace std;

class MergeSort{
    public:
        MergeSort(int, int*);
        void ordenar();
        void mostrarElementos();
        void desordenar();

    private:
        int tamanno;
        vector<int> datos;
        vector<int> copia;
        void ordenarSubArreglo(int, int);
        void mezclar(int, int, int, int);
        void mostrarSubArreglo(int, int);
};