#include<iostream>
#include"mergesort.cpp"
#include<chrono>

using namespace std;
using namespace std::chrono;

void MostrarArreglo(int arreglo[],int tam){
    for (int i = 0; i < tam;i++){

        cout<<arreglo[i]<<endl;
    }
}
void swap(int *uno, int *dos){
    int aux = *uno;
    *uno = *dos;
    *dos = aux;
}

void bubblesort(int arr[],int n)
{
	for (int i = 0; i < n - 1; i++){
        for(int j=0; j < n - i - 1; j++){
            if (arr[j] > arr[j + 1]){
                swap(&arr[j], &arr[j+1]);
            }

        }
    }
}


int main(){
    int tam;

    cout<<"ingrese el tamaño del arreglo"<<endl;
    cin>>tam;

    int arreglo [tam], copia[tam];

    for (int i = 0; i < tam; i++){
        cout<<"ingrese el valor de la posicion numero "<< i << endl;
        cin>>arreglo[i];
        copia[i] = arreglo[i];
    }

    MergeSort mergersort(tam, arreglo);

    int opcion;
    while (true){
        cout<< "ingrese la opcion que desea: "<<endl;
        cout<< "(1) mostrar arreglo sin ordenar"<<endl;
        cout<< "(2) mostrar arreglo ordenado mediante algoritmo burbuja"<<endl;
        cout<< "(3) mostrar arreglo ordenado mediante algoritmo mergesort"<<endl;
        cout<< "(4) mostrar el tiempo de ejecucion de los algoritmos"<<endl;
        cout<< "(5) salir"<<endl;
        cin>>opcion;
        if (opcion == 1){
            MostrarArreglo(arreglo, tam);
        }
        if (opcion == 2){
            bubblesort(arreglo, tam);
            MostrarArreglo(arreglo, tam);	

            for (int i = 0; i < tam; i++) {
                arreglo[i] = copia[i];
            }
        }
        if (opcion == 3){
            cout << "mergesort sin ordenar: " << endl;
            mergersort.mostrarElementos();
            mergersort.ordenar();
            cout << "mergesort ordenado: " << endl;
            mergersort.mostrarElementos();
            mergersort.desordenar();

        }
        if (opcion == 4){
            auto comienzo_bubble = high_resolution_clock::now();
            bubblesort(arreglo, tam);
            auto termino_bubble = high_resolution_clock::now();
            auto duracion_bubble = duration_cast<microseconds>(termino_bubble - comienzo_bubble);
            cout << "el algoritmo bubblesort se demoró: " << duracion_bubble.count() << endl;

            auto comienzo_mergesort = high_resolution_clock::now();
            mergersort.ordenar();
            auto termino_mergesort = high_resolution_clock::now();
            auto duracion_mergesort = duration_cast<microseconds>(termino_mergesort - comienzo_mergesort);
            cout << "el algoritmo mergesort se demoró: " << duracion_mergesort.count() << endl;

            for (int i = 0; i < tam; i++) {
                arreglo[i] = copia[i];
            }

            mergersort.desordenar();

        }   
        if (opcion == 5){
            return 0;
        }
    }
    return 0;
}
